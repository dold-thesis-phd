- Fix overfull hboxes
- Check notation for numbers
- consistency of list capitalization
- include auditor tables in 4.3 (Space usage by database table)
- auditor db diagrams?
- Proof-read references (capitalization in titles)
- transition to chapter 5?
- connection of BSC to Taler (at end of Chapter?)


Table 4.4:
You talk about the response sizes being relevant here for the RTT. Would
be great if we could have them in the table as well (ideally request +
response sizes!). Right now, I do not see bandwidth utilization
_anywhere_ in the thesis!


----------------

Comments about comments:

Table 4.2:
why would a client ever call "rsa private key get public"? Clients
shouldn't even have RSA private keys!??
=> this is called as part of the testbed / setup

-----------------------

Comments I don't understand or can't fix:

p. 15, I appreciated the short and rather harsh critique of blockchains. How do you
explain their meteoric rise (if you see it that way) despite these facts? An nonspecialist
article that adequately explained this “paradox” would be cool, maybe targeted for The
Atlantic.

p. 155-157. The Conclusion, and similar idea from the Introduction, are quite powerful.
I would repeat the suggestion we could really use a nonspecialist article, in a venue like
The Atlantic, on approaches for payment and their is socio-political implications.

p. 43, paragraph 3. I would not regard the use of oracles in game-based definitions as an
extension of Turing machines. However you might formalize the adversary’s computation
(in a RAM model, as a program in some programming language, whatever), we can no
doubt embellish that model by adding oracles. Turing machines are perhaps the most
awkward way of doing it!

p. 43, paragraph beginning “While oracles”. I would, similarly, regard oracles as even
less related to interactive protocols. At least the way that I use this term, interactive
protocols are stylized two-party interactions used for defining the complexity class IP.
They were originally defined, rather informally, with interactive Turing Machines. Better
expositions eliminated that language.

p. 48–57. I think it would be a Herculian job to truly verify this syntax and these games,
and I won’t really try to do so. Maybe you can tell me how these evolved and were
debugged.

p. 84–85, tipping is normally by a customer to a merchant, not the other way around ;-)


44. pp. 123–154. I liked this chapter, but it did feel somewhat out of place compared to the
rest of the thesis. It still carried some vestiges of being a paper (for example, the chapter
speaks a couple of time of its being a paper, rather than a chapter ), and read like one.
The writing seemed to assume more of the user, and it was a bit disorganized compared
to the rest of the presentation. Now I have never felt that a dissertation needed to be all
that unified to be good (theses that amalgamate vaguely related papers are fine by me),
so this this isn’t a big deal. But it might help to switch the order of Chapters 5 and 6,
as it did feel jarring to go back to go back to GNU Taler with the BSC stuff intervening.
And a little bit more of a transition to the current Chapter 5 would be good.


Fixed citations:
(I've left the old papers in there as well)

20. p. 41 and later. The wrong papers ([Poi05], [Sho04], [Cor00]) are being credited for
4provable security, the notion of which is usually credited go [GM82/GM85] (although
credit should arguably be shared more broadly with Blum and Yao, for example). Only
one of the papers you’re siting here is even a survey.



23. p. 42. [Lin17] is not an appropriate reference of the idea of simulation-based definitions.
The idea might be credited to GMR85/89 (zero-knowledge).

