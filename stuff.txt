priorities:
* impl eval (transaction rate!)

stretch goals:
* nice system diagram for implementation scaling (incl. load balancing)
* put things in side-boxes
* taler libtool versioning in impl chapter?
* add blockchain discussion / newer related work to byz consensus
* notation index
* glossary (copy from docs.taler.net)
* protocol diagrams for tipping
* illustration for the browser-based payment process
* illustration for contract terms etc.
* full reproducible setup (describe Guix setup and configure flags) in appendix
* benchmark that shows DB perf doesn't degrade
* talk about fungibility vs blockchain somewhere?
* screenshots in Firefox

--------------------------
Q&A:

The introduction mentions that Taler is "socially and ethically responsible".
Based on what theory of ethics and society?

In some places, shouldn't it be revenue instead of income?
For taxation income (and income transparency matters), but the exchange
does only learn revenue!

what about sequence diagrams for Taler payments?

should the simple diagram be in the intro?
=> it fits better with the design chapter

where do we explain wire plugins?
=> in the exchange implementation section

where do we show how expensive refresh is in terms of the time needed?
=> There is one single comparison (0% vs 10%) plus base latency,
   is that enough?

should the implementation chapter's related work moved to the general related
work chapter?

is KYC explained properly?
=> merchant

should we really include the DB schema?  it doesn't really fit with the
level of detail we have
=> would be better
=> exchange GC

where should we explain the wire fee amortization?
=> low level, => impl

Should there be something about the coin selection in the wallet implementation
section, or is the coinsim discussion enough?
=> refer to coinsin

where do we define what "origin" means in a web context?
=> now defined with reference to RFC in the resource-based payment process explanation

would it not be better if the exchange knew about the amount for a contract
and could refund automatically if the payment wasn't completed before the pay deadline?
=> No, since the customer can complete the payment themselves

Should there be some diagram showing the contents for the
contract terms + deposit modalities

Do we support in the implementation that different deposits are unlinkable
from the contract?  It does not currently look like it, we mix both cases,
leading to some confusion.
=> yes, wallet support required

exchange api version
=> describe

should we use headers instead of extended fulfillment URL?
=> NO, for forward compat reasons, as well as privacy and simplicity


Should I have a notation index?
=> yes

Should I have a glossary for terms like "contract", "offer", "contrac terms", etc.?

What about deanonymization with the link protocol?
Where is the discussion?
=> The customer can only be deanonymized with link
   if they voluntarily give up their private key
   to the exchange, or if the malicious merchant
   cooperates with the exchange to deanonymize

Where do we state "what prevents tax evasion"?
=> ?

Where is the refresh/linking protocol described on a high level?

Should we rename RefreshRequest to RefreshPrepare?
=> Not really necessary IMHO

Where do we describe the indian merchant scenario?

Where do we clearly state the security assumptions used in practice?
=> don't use tor as example

Does the customer check that the exchange gave the same
gamma when the protocol is replayed?
=> Fixed now, needed to be added

Should we get rid of the X- prefix?
=> Yes!

Are the reasons for a refresh protocol is essential clearly explained?
=> I think so! (FD)

Are practical anonymity concerns discussed properly (amounts, denomination, contrac terms, ...)?

/track/transaction is horribly named, it should be /track/deposit
=>  No!  This is not the right answer, as it does not track deposits,
    but rather all deposits associated with a h_contract_terms

- should the RSA blind signature algorithm be shown somewhere?
=> yes, complete concrete instantiation


- salting of wire hash info?
=> should not happen in the "/wire" response

- on-line vs online
=> "current usage favors online", so we use that


/payback deiscussion
=> maybe

do we discuss the issue of withdraw limits?

--------------------------

- the resource URL should *always* be specified
  when paying for resources, as otherwise the fallback
  to taler:pay does not work!

- NFC
 - indian merchant scenario using KYC hash

- KYC registration
 - backend automatically registers
 - auditor part

- auditor assumptions:  has replica, db is imported first
 - auditor: multisig

- discuss: different cases, who has internet, merchant or user
 - merchant registration over customer channel to exchange in some cases

- maybe move general requirements into introduction

- writing Taler vs GNU Taler
  => always write GNU Taler?

- byz consensus chapter does *not* currently contain any references
  to the blockchain stuff.  it should discuss the application in more detail

- mention explicitly that taler does not do automatic tax collection

- limits on withdrawal, where, how?  is the auditor involved?

- can/should the merchant cover refresh fees, to hide them from the user?
  (we can't set them to zero, because DoS attacks)

- benchmarking:  shouldn't we also show that the resource usage for *clients* is acceptable?

- what does the exchange do when it maxed out the limit for denomination keys?

- we need to explain how BSC and the ledger work together somewhere!  graphics/diagram!

- discuss practical anonymity somewhere
  (amounts, denominations, contract terms, Tor)
