\chapter{Résumé en Français}

Les nouveaux protocoles de réseautage et cryptographiques peuvent
considérablement améliorer les systèmes de paiement électroniques en ligne. Le
présent mémoire porte sur la conception, la mise en œuvre et l'analyse
sécuritaire du GNU Taler, un système de paiement respectueux de la vie privée
conçu pour être pratique pour l'utilisation en ligne comme méthode de
(micro-)paiement, et en même temps socialement et moralement responsable.

La base technique du GNU Taler peut être dû à l'e-cash de David Chaum.  Notre
travail va au-delà de l'e-cash de Chaum avec un changement efficace, et la
nouvelle notion de transparence des revenus garantissant que les marchands ne
peuvent recevoir de manière fiable un paiement d'un payeur non fiable que
lorsque leurs revenus du paiement est visible aux autorités fiscales.

La transparence des revenus est obtenue grâce à l'introduction d'un protocole
d'actualisation donnant lieu à un changement anonyme pour un jeton
partiellement dépensé sans avoir besoin de l'introduction d'une évasion fiscale
échappatoire.  En outre, le protocole d'actualisation peut être utilisé pour la
mise en œuvre des swaps atomiques de style Camenisch, et pour la préservation
de l'anonymat en présence d'annulation du protocole et d'erreurs de crash avec
perte de données par les participants.  De plus, nous démontrons la sécurité
prouvable de la transparence anonyme de nos revenus e-cash, qui concerne en
plus l'anonymat habituel et les propriétés infalsifiables de l'e-cash, ainsi
que la conservation formelle des fonds et la transparence des revenus.

Notre mise en œuvre du GNU Taler est utilisable par des utilisateurs
non-experts et s'intègre à l'architecture  du web moderne.   Notre plateforme
de paiement aborde une série de questions pratiques telles que la prodigue des
conseils aux clients, le mode de remboursement, l'intégration avec les banques
et les chèques ``know-your-customer (KYC)'', ainsi que les exigences de
sécurité et de fiabilité de la plateforme web.  Sur une seule machine, nous
réalisons des taux d'opérations qui rivalisent avec ceux des processeurs de
cartes de crédit commerciaux globaux.  Nous améliorons la robustesse des
échanges - la composante qui détient l'argent de banque en mains tierces en
échange de l'e-cash - en ajoutant une composante d'auditeur qui vérifie le
fonctionnement correct du système, et permet une détection tôt d'un compromis
ou d'un mauvais comportement de l'échange. 

Tout comme les comptes bancaires ont raisons d'exister de même que les billets
de banque, l'e-cash ne sert que dans le cadre d'un empilement  de système de
paiement. Les registres distribués ont récemment gagné une immense popularité
en tant que substituant potentiel des parties de l'industrie financière
traditionnelle.

Pendant que les crypto-monnaies basées sur la preuve de travail à l'instar de
Bitcoin doivent encore être mises à l'échelle pour servir de substituant aux
systèmes de paiement établis, d'autres systèmes plus efficaces basés sur les
Blockchains avec des algorithmes de consensus plus classiques pourraient avoir
des applications prometteurs dans le secteur financier. Nous faisons dans la
conception, la mise en œuvre et l'analyse de la Byzantine Set Union Consensus,
un protocole de Byzantine consensus qui s'accorde sur un (Super-)ensemble
d'éléments à la fois, au lieu d'accepter en séquence les éléments individuels
sur un ensemble. Byzantine Set consensus peut être utilisé comme composante de
base pour des chaînes de blocs de permissions, où (à l'instar du style Nakamoto
consensus) des blocs entiers d'opérations sont convenus à la fois d'augmenter
le taux d'opération.

\section*{Objectifs du GNU Taler}
Nous avons entamé la conception du GNU Taler avec un ensemble d'objectifs de
conception de haut niveau.  Ces objectifs sont classés par ordre de leur
importance,  et lorsqu'un compromis doit être fait, celui qui apporte son
soutien à l'objectif le plus haut classé est préféré : 

\begin{enumerate}
  \item \textbf{GNU Taler doit être mise en œuvre comme un logiciel libre.}
    Le vocable libre ici renvoie à ``liberté, comme dans liberté d'expression''
    et non ``gratuité, comme dans bière gratuite''. Plus particulièrement, les
    quatre libertés essentielles du logiciel libre doivent être respectées,
    notamment que les utilisateurs doivent avoir la liberté (1) d'exécuter le
    logiciel, (2) l'étudier et le modifier, (3) redistribuer des copies, et (4)
    distribuer des copies de la version modifiée.

    En ce qui concerne les marchands, cela empêche le verrouillage par le
    fournisseur, car un autre fournisseur de paiement peut prendre le relais,
    si l'actuel offre une qualité de service insuffisante. Comme le logiciel de
    prestataire de paiement lui-même est libre, les plus petits pays ou
    organisations défavorisés peuvent exécuter le système de paiement sans
    besoin d'être contrôlés par une société étrangère.  Les clients bénéficient
    de cette liberté, car le portefeuille électronique peut être conçu pour
    fonctionner sur une variété de platesformes, et des fonctionnalités
    hostiles à l'utilisateur telles que le tracking ou la télémétrie pourrait
    facilement être supprimé du portefeuille électronique.

    Cela exclut l'utilisation obligatoire du matériel informatique spécialisé tel
    que les cartes électroniques ou d'autres hardware security modules, car le
    logiciel qu'ils exécutent ne peut être modifié par l'utilisateur.

    Cependant, ces composantes peuvent être volontairement utilisées par des
    marchands, clients ou organismes de paiement dans le but d'améliorer leur
    sécurité opérationnelle.

\item \textbf{GNU Taler doit protéger la vie privée des acheteurs.}

    La protection de la vie privée devrait être garantie par des mesures
    techniques, aux antipodes des simples politiques.  Ceci particulièrement à
    l'aide des micropaiements pour des contenus en ligne, une quantité
    disproportionnée de données plutôt privées sur les acheteurs serait
    révélée, si le système de paiement n'a pas de mécanisme de protection de la
    vie privée.

    Dans les législations relatives à la protection des données (à l'instar du
    GDPR récemment introduit en Europe), les marchands en profitent également,
    car aucune violation des données des clients ne peut se produire si ces
    informations ne sont pas collectées premièrement par conception. De toute
    évidence, certaines données privées, telles l'adresse de livraison pour une
    livraison physique, doivent toujours être collectées en fonction des
    besoins de l'entreprise.



    La sécurité des systèmes de paiement en profite également, car le modèle passe
    de l'authentification des clients à la simple autorisation de paiement.
    Cette approche exclut les classes entières d'attaques telles que
    l'hameçonnage ou la fraude par carte de crédit.

  \item \textbf{GNU Taler doit permettre à l'État de procéder à l'imposition
    des revenus et la répréhension des activités commerciales illégales.}


    Comme un système de paiement doit toujours être légal pour son fonctionnement
    et utilisation, il doit se conformer aux exigences susmentionnées. En
    outre, nous considérons que la perception des impôts est bénéfique pour la
    société.

  \item \textbf{GNU Taler doit prévenir la fraude.}

    Cela impose des exigences sur la sécurité du système, ainsi que sur la
    conception générale, car la fraude de paiement peut également se produire par
    la conception illusoire de l'interface utilisateur, ou le manque de preuves
    cryptographiques pour certains processus.

  \item \textbf{ GNU Taler ne doit permettre uniquement que la divulgation de la quantité minimale d'informations nécessaires.}

    La raison derrière cet objectif est semblable à (2).  La vie privée des
    acheteurs est prioritaire, mais d'autres parties à l'instar des marchands en
    profitent encore, par exemple en conservant des détails sur leurs finances
    cachés aux concurrents.

  \item \textbf{GNU Taler doit être utilisable.}


    Il doit particulièrement être utilisable pour des clients non-experts. Cette
    utilité s'applique également à l'intégration avec les marchands, et renseigne
    sur les choix concernant l'architecture, telles que les procédures
    d'encapsulation requièrent des opérations cryptographiques dans une composante
    isolée avec une simple API.


  \item \textbf{GNU Taler doit être efficace.}

    Les approches telles que la preuve de travail sont exclues par cette exigence.
    L'efficacité est nécessaire pour que GNU Taler soit utilisé pour les
    micropaiements.


  \item \textbf{GNU Taler doit éviter les points de défaillance uniques.}

    Alors que la conception à présenter plus tard est plutôt centralisée, son
    objectif demeure d'éviter les points uniques de défaillance. Cela se manifeste
    dans les choix architecturaux tels que l'isolement de certaines composantes et
    procédures d'audit.

  \item \textbf{GNU Taler doit promouvoir la concurrence.}


    Le processus d'intégration des concurrents aux systèmes doit  relativement être
    facile.  Alors que les obstacles à ces systèmes financiers traditionnels sont
    assez nombreux, le fardeau technique d'adhésion aux nouveaux concurrents doit
    être minimisé. Un autre choix de conception qui soutient ceci est de diviser
    l'ensemble du système en de plus petites composantes pouvant être exploitées,
    développées et améliorées indépendamment, au lieu d'avoir un système
    complètement monolithique.

\end{enumerate}

\section*{Byzantine Set Union Consensus}

Le protocole de Byzantine Set Union Consensus que nous procédons à la
conception, la mise en œuvre et l'évaluation, offre une amélioration
asymptotique au-delà d'une mise en œuvre naïve à l'aide de la machine de
duplication de l'État.

Pour des pairs $n$ et un ensemble d'éléments $m$, la messagerie de notre protocole
est d'une complexité $O(mn + n^2)$ lorsqu'aucun pair ne montre le comportement
Byzantine. Lorsque les pairs $f$ montrent un comportement Byzantine, la
complexité du message est $O(mnf + kfn^2)$, où k est le nombre d'éléments
valides exclusivement disponibles pour l'adversaire.

Nous démontrons comment $k$ peut être délimité pour des applications pratiques
communes, puisqu'en général k est seulement délimité par la largeur disponible
à l'adversaire. En pratique, on s'attend à ce que k f soit significativement
plus petit que m.  Ainsi, $O (mnf + kfn^2)$ est une amélioration par rapport à
l'utilisation de SMR-PBFT (duplication de la machine d'État avec la tolérance
de faille Byzantine pratique) qui aurait la complexité $O(mn^2)$ sous ces
hypothèses. 

Nous parvenons à ce résultat en combinant un protocole de Byzantine Consensus
existant au protocole de réconciliation efficace d'Eppstein. La même
construction s'applique également aux autres protocoles de consensus.

\section*{Contributions}

Nous revendiquons les contributions clés ci-dessous dans le cadre ce mémoire :

\begin{itemize}
\item Nous procédons à la conception, la mise en œuvre et une analyse efficace
du Byzantine consensus protocol sur des structures définies permettant une mise
en œuvre optimisée des registres d'opérations distribuées.
\item Nous introduisons la notion de transparence des revenus pour l'e-cash,
avec une instanciation en e-cash et des épreuves du style Chaum.
\item Nous procédons à la conception du système de paiement GNU Taler en tenant
compte des aspects pratiques de l'e-cash, notamment les annulations, les
défaillances de réseau, les remboursements, les paiements multi-coin, les
défauts de synchronisation des portefeuilles et leurs effets sur l'anonymat;
montrant la nécessité d'une opération d'actualisation.
\item Nous proposons une modification de notre protocole offrant une protection
contre certains scénarios de chantage et d'enlèvement.
\item Nous procédons à la conception et la mise en œuvre d'une intégration
homogène et native de l'e-cash dans l'architecture du web, et discutons des
aspects de sécurité et de confidentialité de cette intégration.
\item Nous avons mis en œuvre le système de paiement GNU Taler et avons évalué
ses performances.
\end{itemize}
