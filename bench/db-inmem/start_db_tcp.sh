#!/usr/bin/env bash
nohup postgres -D /dev/shm/talerdb-inmem/ -k 5432 >/dev/shm/talerdb-inmem/nohup.out &
disown $!
