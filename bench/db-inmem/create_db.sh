#!/usr/bin/env bash

set -eu

mkdir -p $HOME/sockets
initdb -D /dev/shm/talerdb-inmem -U $USER
cp postgresql.conf /dev/shm/talerdb-inmem/
