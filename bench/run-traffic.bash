#/usr/bin/env bash


export GNUNET_BENCHMARK_DIR=$(readlink -f ./stats)
mkdir -p results

ip -s link show enp4s0f0 > results/traffic-before.txt

taler-exchange-benchmark -c benchmark-remote-gv.conf -m client -p 1 -n 10000 >& /dev/shm/benchmark.log
cp /dev/shm/benchmark.log results/traffic.log

ip -s link show enp4s0f0 > results/traffic-after.txt
