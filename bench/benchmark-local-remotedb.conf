# This file is in the public domain.
#
[paths]
# Persistant data storage for the testcase
# This value is a default for `taler_config_home'
taler_test_home = exchange_benchmark_home/

[taler]
# Currency supported by the exchange (can only be one)
currency = EUR

[exchange]
# how long is one signkey valid?
signkey_duration = 4 weeks
# how long are the signatures with the signkey valid?
legal_duration = 2 years
# how long do we provide to clients denomination and signing keys
# ahead of time?
lookahead_provide = 4 weeks 1 day
# HTTP port the exchange listens to
port = 8082
# Master public key used to sign the exchange's various keys
master_public_key = 98NJW3CQHZQGQXTY3K85K531XKPAPAVV4Q5V8PYYRR00NJGZWNVG
# How to access our database
DB = postgres
# Base URL of the exchange. Must be set to a URL where the
# exchange (or the twister) is actually listening.
base_url = "http://localhost:8082/"
# Keep it short so the test runs fast.
lookahead_sign = 12 h

[exchangedb-postgres]
config = "postgres://bench:bench@192.168.42.2:5433/taler"

[auditordb-postgres]
config = "postgres://bench:bench@192.168.42.2:5433/taler"

[benchmark-remote-exchange]
# Only used for SSH connection
host = 
dir =

[account-2]
# What is the bank account (with the "Taler Bank" demo system)?
URL = "payto://x-taler-bank/localhost:8081/2"
# This is the response we give out for the /wire request.  It provides
# wallets with the bank information for transfers to the exchange.
wire_response = ${TALER_CONFIG_HOME}/account-2.json
# Which wire plugin should we use to access the account?
plugin = taler_bank
# Authentication information for basic authentication
taler_bank_auth_method = "basic"
username = user
password = pass

enable_debit = YES
enable_credit = YES

[fees-x-taler-bank]
# Fees for the forseeable future...
# If you see this after 2017, update to match the next 10 years...
wire-fee-2018 = EUR:0.01
wire-fee-2019 = EUR:0.01
wire-fee-2020 = EUR:0.01
wire-fee-2021 = EUR:0.01
wire-fee-2022 = EUR:0.01
wire-fee-2023 = EUR:0.01
wire-fee-2024 = EUR:0.01
wire-fee-2025 = EUR:0.01
wire-fee-2026 = EUR:0.01
wire-fee-2027 = EUR:0.01

closing-fee-2018 = EUR:0.01
closing-fee-2019 = EUR:0.01
closing-fee-2020 = EUR:0.01
closing-fee-2021 = EUR:0.01
closing-fee-2022 = EUR:0.01
closing-fee-2023 = EUR:0.01
closing-fee-2024 = EUR:0.01
closing-fee-2025 = EUR:0.01
closing-fee-2026 = EUR:0.01
closing-fee-2027 = EUR:0.01

# Sections starting with "coin_" specify which denominations
# the exchange should support (and their respective fee structure)
[coin_eur_ct_1]
value = EUR:0.01
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.00
fee_deposit = EUR:0.00
fee_refresh = EUR:0.01
fee_refund = EUR:0.01
rsa_keysize = 2048

[coin_eur_ct_10]
value = EUR:0.10
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 2048

[coin_eur_1]
value = EUR:1
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 2048

[coin_eur_5]
value = EUR:5
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 2048

[coin_eur_10]
value = EUR:10
duration_overlap = 5 minutes
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 2048
