#/usr/bin/env bash


export GNUNET_BENCHMARK_DIR=$(readlink -f ./stats)

rm -rf stats
taler-exchange-benchmark -c benchmark-local.conf -p 1 -n 10000 >& /dev/shm/benchmark.log
mkdir -p "results/stats-1"
cp -a stats "results/stats-1"/
cp /dev/shm/benchmark.log "results/stats-1/"
