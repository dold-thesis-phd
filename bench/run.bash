#/usr/bin/env bash

set -eu
set -x

ulimit -c 4096

export GNUNET_BENCHMARK_DIR=$(readlink -f ./stats)
export GNUNET_FORCE_LOG=";;;;ERROR"

DB=postgresql:///postgres?host=/home/dold/sockets

for x in $(seq 10 10 190) $(seq 200 100 2000); do
	if [[ -d "results/stats-$x" ]]; then
		echo skipping existing results/stats-$x
		continue
	fi
	echo running with $x clients

	psql "$DB" -c "drop database if exists taler;"
	psql "$DB" -c "create database taler;"
	echo created db

	rm -rf stats
	taler-exchange-benchmark -c benchmark-local.conf -p $x -n 1000 >& /dev/shm/benchmark.log
	mkdir -p "results/stats-$x"
	cp -a stats "results/stats-$x"/
	tail /dev/shm/benchmark.log > "results/stats-$x/benchmark.log"
done	
